require "spec_helper"

RSpec.describe Rcheck::Request do
  context "#make_requests/1" do
    let(:website) { "https://about.gitlab.com" }
    it "makes the correct number of requests" do
      stub_request(:any, website)

      pings = Rcheck::Request.make_requests({:time => 4, :duration => 8, :website => website})

      # This is to make sure that a mock exists
      expect(WebMock).to have_requested(:any, website).twice
      expect(pings.count).to be 2
    end
  end
end
