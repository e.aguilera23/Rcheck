module Rcheck
  module Request
    module_function

    # Makes the necessary requests
    # +opts+: It can contain
    #   - +duration+: How many times a request will be made.
    #   - +time+: Time interval between requests
    #   - +website+: Website to request. this is only relevant inside Ping object
    #   - +verbose+: Flag to print info every request
    def make_requests(opts)
      time = opts[:time]
      duration = opts[:duration] / time
      pings = []

      duration.times do |n|
        ping = Ping.new(opts[:website], n + 1)

        ping.run
        pings << ping

        if opts[:verbose]
          Rcheck::CLI.print_verbose(ping)
        end

        sleep(time)
      end

      pings
    end
  end
end
