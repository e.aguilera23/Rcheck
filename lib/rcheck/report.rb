require 'csv'
module Rcheck
  module Report
    module_function
    # == Rcheck::Report Module
    # This module handles the logic for the reports to be made.
    #   - average calculaion (#average/1)
    #   - csv generation (#csv/2)
    #   - evaluation of a request to express its result (#verbose/1)

    # This functions isolates the logic behind getting the average of a series of requests
    # Returns the average time in **milliseconds**
    # params:
    #   - +pings+: Ping objects
    def average(pings)
      pings.reduce(0) { |acc, p| acc + p.time } / pings.count
    end

    # Creates a csv file with the data of each request made
    # params:
    #   - +pings+: Ping objects with metadata
    #   - +filename+: Path to create the file
    def csv(pings, filename)
      unless filename.include?(".csv")
        filename = filename + ".csv"
      end

      CSV.open(filename, "wb") do |csv|
        csv << pings.first.attrs_name_array
        pings.each do |p|
          csv << p.info_array
        end
      end
    end

    # Shows the status of a request. If it has a 200 code will be a success.
    # Any other code will show only the result.
    # params:
    #   - +ping+: Ping object with request metadata
    def verbose(ping)
        message = "#{ping.website} has responded with "
        status = ping.code
        time = (ping.time * 100).round(2)

        case status
        when "200"
          message += "success (#{status})"
        else
          message += "code #{status}"
        end

        message
    end
  end
end
