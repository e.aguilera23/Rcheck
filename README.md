# Rcheck

## Introduction

Small Ruby gem that exposes a CLI to check the status of https://gitlab.com or https://about.gitlab.com and reports an average response time after probing the site every 10 seconds for a one minute.

## Table of contents

- [Installation](#Instalation)
	- [As a script](#as-a-script)
	- [As a gem](#as-a-gem)
- [Usage](#usage)
	- [Command Line](#command-line)
	- [Gem](#gem)
	- [Options](#options)
- [Testing](#testing)
	- [Unit Tests](#unit-tests)
	- [Acceptance Tests](#acceptance-tests)
- [Documentation](#documentation)
	- [Data Flow Diagram](#data-flow-diagram)
	- [Class Diagram](#class-diagram)
- [License](#license)

## Installation

There are 2 ways to install this gem after cloning it:

### As a script
```
cd rcheck/
bundle install
```

### As a gem

```
cd rcheck/
rake install

### In a file
require 'rcheck'
```

## Usage

### Command Line

To use this script you can run: `ruby lib/rcheck.rb`. This will start pinging https://about.gitlab.com.

### Gem

You can require this gem to use it inside another program. You only need to call the `Rcheck::CLI.run` function, with an array of options. This array can be empty.

### Options
It accepts this options in both modalities:

  - `ruby lib/rcheck.rb <website>`
  	- You can pass the name of a website to change the target
  	- The default is `https://about.gitlab.com`
  - `-v --verbose`
  	- Shows information of every request
  - `-t --time TIME`
  	-  Set time between requests in seconds. (Default to 10 sec.)
  - `-d --duration DURATION`  
  	-  Set total time spending on making requests in seconds. (Default to 60 seconds)
  - `--csv FILENAME`          
  	-  Exports data of each request into a csv file
  - `-h --help`               
  	-  Show this options as help

## Testing

 This program has 2 kinds of tests, behaviour and unit testing. There is a bash script to choose which kind of tests you want to run.
 
 To run the script you need to:
 
 ```
 // Give it execution mode
 chmod 700 test.sh
 
 // Run unit tests
 ./test.sh unit
 
 //Run bdd tests
 ./test.sh bdd
 
 // Let it ask you
 ./test.sh
 ```
 
 If you want to run all of them you can do `rspec`

### Unit Tests
 
 Tests that throw an input to the module's functions and expect the same output every time. This tests are **fast** and can run repeatedly without slowing down the development process. This kind of test doesn't interact with external services.
 
### Acceptance Tests
 
 Test that the interaction between the user and the program runs accordingly as expected. This tests are **slow** because they depend on making a variety of requests and waiting for them.
 
## Documentation
 
 Every relevant method is documented inside the code.
 
### Data Flow Diagram
 
 This diagram shows how the data flows through the program, explaining how the modules communicate between each other. 
 
 Read left to right and top to bottom.
 
![data-flow-diagram](./docs/DataFlowDiagram.jpg)
 
### Class Diagram
 
![](./docs/ClassDiagram.jpg)

## License

The gem is available as open source under the terms of the [MIT License](https://opensource.org/licenses/MIT).
