
lib = File.expand_path("../lib", __FILE__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)
require "rcheck/version"

Gem::Specification.new do |spec|
  spec.name          = "rcheck"
  spec.version       = Rcheck::VERSION
  spec.authors       = ["Eduardo Aguilera"]
  spec.email         = ["laloao.2302@gmail.com"]

  spec.summary       = %q{Checks the availability of a website and reports its response time}
  spec.license       = "MIT"

  spec.files         = `git ls-files -z`.split("\x0").reject do |f|
    f.match(%r{^(test|spec|features)/})
  end
  spec.bindir        = "exe"
  spec.executables   = spec.files.grep(%r{^exe/}) { |f| File.basename(f) }
  spec.require_paths = ["lib"]
  spec.required_ruby_version= ">= 2.3.0"

  spec.add_development_dependency "bundler", "~> 1.16"
  spec.add_development_dependency "rake", "~> 10.0"
  spec.add_development_dependency "rspec", "~> 3.0"
  spec.add_development_dependency "aruba", "~> 0.14.5"
  spec.add_development_dependency "cucumber", "~> 1.3.20"
  spec.add_development_dependency "webmock", "~> 3.3.0"
end
